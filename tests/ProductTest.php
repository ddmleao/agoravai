<?php 

namespace App\Models;

class ProductTest extends \PHPUnit\Framework\TestCase
{
	
	public function testProductType()
	{
		$product = new Product();
		$this->assertInstanceOf(ProductInterface::class, $product)
	}

	public function testProductName()
	{
		$product = new Product();
		$product->setName('Sabao');
		$this->assertEquals($product->getName(), "Sabao");
	}
}

?>